<?php

// 4 scaler data type
//1: boolean

$bool=True;
echo $bool;

// integer
$a=1089;
echo "<br>".$a;
$b=-509;
echo "<br>".$b;

// float
$f1=3.77;
echo "<br>".$f1;

//4 type string

// single quated
echo 'this is a single string<br>';

// double quated
echo "this is a single string<br>";
$str=<<<EOD
EOD;

echo $str;


//Array
$person=array(

    array("Sultana",20,5.3),
    array("siddika",21,5.2),
    array("Sohana",30,5.1),
    array("katti",22,5.8),
    array("nur",26,5.9),
    array("Sultana siddika",25,5.3),
);
    echo $person[0][0] . " " . $person[0][1]. "<br>";



$person=array(

    array("Name"=>"Sultana","Age"=>20,"Height"=>5.3),
    array("Name"=>"katti","Age"=>20,"Height"=>5.3),
    array("Name"=>"katti","Age"=>20,"Height"=>5.3),
    array("Name"=>"Sultana","Age"=>20,"Height"=>5.3),

);
echo $person[2]["Name"] . " " . $person[2]["Age"] . "<br>";


//2.object

class person{
    public $name;
    public $age;
    public $height;
    public function showPersonInformation(){
        // show data
    }
}
$personObject=new person();
echo "<pre>";
var_dump($personObject);
echo "<pre>";

?>